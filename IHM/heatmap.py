import os
import json
import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage import gaussian_filter

def generate_heatmap(mouse_positions, width, height):
    heatmap = np.zeros((height, width))

    for pos in mouse_positions:
        x, y = pos['x'], pos['y']
        if(x<width and y<height):
            heatmap[y, x] += 1

    return heatmap

def plot_heatmap(heatmap, title):
    plt.imshow(gaussian_filter(heatmap, sigma=5), cmap='hot', interpolation='nearest')
    plt.title(title)
    plt.colorbar()
    plt.show()

def save_heatmap(heatmap, title, save_path):
    plt.imshow(gaussian_filter(heatmap, sigma=15), cmap='hot', interpolation='nearest')
    plt.title(title)
    plt.colorbar()
    plt.savefig(save_path)
    plt.close()

# Dictionary to accumulate heatmaps for each experiment
heatmap_per_experiment = {}

# Iterate through all JSON files in the current folder
for json_file_name in os.listdir('.'):
    if json_file_name.startswith('interaction-data-') and json_file_name.endswith('.json'):
        print(json_file_name)
        with open(json_file_name, 'r') as json_file:
            experiment_data = json.load(json_file)

        experimenter_name = experiment_data['experimenterName']

        # Loop through each experiment and accumulate data
        for experiment_name, experiment_info in experiment_data['experiments'].items():
            height = experiment_info["height"]
            width = experiment_info["width"]
            mouse_positions = experiment_info["mousePositions"]

            # Generate or update the heatmap for the experiment
            if experiment_name not in heatmap_per_experiment:
                heatmap_per_experiment[experiment_name] = generate_heatmap(mouse_positions, width, height)
            else:
                heatmap_per_experiment[experiment_name] += generate_heatmap(mouse_positions, width, height)

# Save and/or plot the accumulated heatmaps for each experiment
for experiment_name, heatmap in heatmap_per_experiment.items():
    title = f"Combined Heatmap - {experiment_name}"
    save_path = f"combined_heatmap_{experiment_name}.png"
    
    #plot_heatmap(heatmap, title)  # Display heatmap
    save_heatmap(heatmap, title, save_path)  # Save heatmap to a file
