import os
import json
import pandas as pd
import math

# Directory containing all JSON files
json_files_directory = '.'


experiment_data_list = []
number_experimenters = 0

# Iterate through each JSON file in the directory
for filename in os.listdir(json_files_directory):
    if filename.startswith('interaction-data-') and filename.endswith('.json'):
        # Extract experimenter's name from the file name
        experimenter_name = filename.replace('interaction-data-', '').replace('.json', '')

        # Load JSON data for each experimenter
        with open(os.path.join(json_files_directory, filename)) as json_file:
            data = json.load(json_file)

        number_experimenters += 1
        experimenter_row = {'experimenter_name': experimenter_name}

        # Iterate through experiments for each experimenter
        for experiment_name, experiment_data in data['experiments'].items():
            # Extract relevant information
            duration = experiment_data['experimentDuration']
            click_count = experiment_data['clickCount']
            cursor_movement = len(experiment_data['mousePositions'])

            # Save data in the experimenter_row dictionary
            experimenter_row[f'experiment_{experiment_name}_duration'] = math.floor(duration/1000)
            experimenter_row[f'experiment_{experiment_name}_click_count'] = click_count
            experimenter_row[f'experiment_{experiment_name}_mouvement'] = cursor_movement

        # Append experimenter_row to experiment_data_list
        experiment_data_list.append(experimenter_row)

# Create a DataFrame from the list of experiment data
df = pd.DataFrame(experiment_data_list)

# Save DataFrame to CSV file
df.to_csv('experiment_data.csv', index=False)
