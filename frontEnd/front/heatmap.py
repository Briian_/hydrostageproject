import matplotlib.pyplot as plt
import numpy as np



# Your x, y positions
x_positions = [pos['x'] for pos in mousePositions]
y_positions = [pos['y'] for pos in mousePositions]

# Create a 2D histogram
heatmap, xedges, yedges = np.histogram2d(y_positions, x_positions, bins=(100, 100))
extent = [yedges[0], yedges[-1], xedges[0], xedges[-1]]

# Plot the heatmap
plt.imshow(heatmap.T, extent=extent, origin='lower', cmap='viridis')
plt.colorbar(label='Click Count')
plt.title('Mouse Click Heatmap')
plt.xlabel('Y Position')
plt.ylabel('X Position')
plt.show()