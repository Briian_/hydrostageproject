import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

import html2canvas from 'html2canvas';
// import { Chart } from 'chart.js/auto'

import 'angular-plotly.js';
import 'plotly.js/dist/plotly.js';



platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
