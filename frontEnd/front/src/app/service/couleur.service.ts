import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CouleurService {

  constructor() { }


  attribuerCouleur(value: any, maxi: number, couleur1: string, couleur2: string){
    let factor, c1, c2 = 0;
    value = Math.abs(value)
    if (value < maxi/3){
      factor = 3*value/maxi;
      c1 = 255;
      c2 = 255 * factor;
    }
    else if(value >= maxi/3 && value < 2*maxi/3){
      factor = (value - maxi/3);
      c1 = (1-factor) * 255;
      c2 = 255;
    }
    else{
      factor = value/maxi;
      c1 = (1-factor) * 255;
      c2 = 255;
    }

    let r = 0; let g = 0; let b = 0;

    if (couleur1=="green" || couleur1=="vert") g = c1;
    else if (couleur1=="blue" || couleur1=="bleu") b = c1;
    else r = c1;

    if (couleur2=="red" || couleur2=="rouge") r = c2;
    else if (couleur2=="blue" || couleur2=="bleu") b = c2;
    else g = c2;

    return 'rgba('+r.toString()+','+g.toString()+','+b.toString()+',1)'
  }
}
