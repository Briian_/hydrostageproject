import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
declare let Plotly: any;
import * as mapboxgl from 'mapbox-gl';
import { Observable, lastValueFrom, map } from 'rxjs';
import { CouleurService } from './couleur.service';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private http: HttpClient, private couleurService: CouleurService) {}

  generateTable(): Promise<any>{
    return lastValueFrom(this.http.get('/osur/display_table', { responseType: 'json' }))
  }

  // plotTable(title: string, plotDiv: string, x: any[], y: Record<string, any>) {
  //   const dataTraces = [];
  
  //   for (const key of Object.keys(y)) {
  //     const values = y[key];
  //     const trace = {
  //       x: x,
  //       y: values,
  //       type: 'scatter',
  //       name: key
  //   };
  
  //     dataTraces.push(trace);
  //   }
  
                    
  //   const layout = {
  //     title: title,
  //     xaxis: {
  //       type: 'date',
  //       title: 'Dates'
  //     },
  //     displaylogo: false,

  //   };
    
  //   Plotly.newPlot(plotDiv, dataTraces, layout);
  // }

  plotTableUnique(title: string, plotDiv: string, x: any[], y: any[], k: string, couleur: string) {
    const values = y;
    const couleurs = values.map((v:any) => this.couleurService.attribuerCouleur(v, 1,'rouge','vert'));
    const trace = {
      x: x,
      y: values,
      mode: 'lines+markers',
      name: k,
      marker: {color: couleurs},
      line: {color : 'grey'},
    };
         
    const layout = {
      title: {
        text: title,
        font: {size: 14}
      },
      xaxis: {
        type: 'date',
        title: 'Dates'
      },
      displaylogo: false,

    };
    
    Plotly.newPlot(plotDiv, [trace], layout);
  }

  getValuesWithKey(d: any, k:any){
    return d[k];
  }

  afficherTableau(nomColonne: string, tabDiv: string, dates: any[], valeurs: any[]){
    const couleurs = valeurs.map((v:any) => this.couleurService.attribuerCouleur(v, 1,'bleu','rouge'));
    const data = [{
      type: 'table',
      columnwidth: [10,10],
      columnorder: [0,1],
      header: {
        values: ['Dates',nomColonne],
        align: ["left", "center"],
        line: {width: 1, color: '#506784'},
        fill: {color: '#555555'},
        font: {family: "Arial", size: 12, color: "white"},
      },
      cells: {
        values: [dates, valeurs],
        align: ["left", "center"],
        line: {color: "#506784", width: 1},
        fill: {color: ['white', couleurs]},
        font: {family: "Arial", size: 11, color: 'black'},
      }
    }]

    Plotly.newPlot(tabDiv, data);
  }
    
}
