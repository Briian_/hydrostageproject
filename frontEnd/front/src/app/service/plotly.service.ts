import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
declare let Plotly: any;
import * as mapboxgl from 'mapbox-gl';
import { BehaviorSubject, Observable, lastValueFrom, map, min, of } from 'rxjs';
import { CouleurService } from './couleur.service';
// import { GeoJsonObject } from 'geojson';
//const bretagne: GeoJsonObject = require('../../../../../bass.json');
import bretagne from '../../../../../communes-bretagne.json';
import { features } from 'process';

@Injectable({
  providedIn: 'root'
})
export class PlotlyService {
  private currentLoc = new BehaviorSubject<string>('');
  private currentID = new BehaviorSubject<string>('');
  private couleur = new BehaviorSubject<string>('');
  private selected = new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient, private couleurService: CouleurService) { }
  
  plotScatter(title: string, plotDiv: string, x:any[], y:any[], loga: boolean){  
    const moment = x.map(item => item.year);
    const values = y.map(item => item.value);
    const couleurs = y.map((v:any) => this.couleurService.attribuerCouleur(v,4,'rouge','vert'));
    let trace = {
      x: x,    
      y: y,   
      type: 'line',
      mode: 'lines',
      line: {color: 'purple'},
      marker: {color: couleurs, size: 2},
      tickmode: 'array',
      tickvals: moment,
      ticktext: moment.map(String) 
    };
                  
    let layout = {
      title:title,

      xaxis: {
        type: 'date',  // Définissez l'axe X comme un axe de type date
        title: 'Dates' // Titre de l'axe des abscisses
      },
      yaxis: {
        type: '',
        title: 'Débit (m³/s)'
      },
      // Autres configurations de layout

    };
    let config = {
      scrollZoom: true, // Désactiver le zoom avec la molette de la souris
      displayModeBar: true, // Désactiver la barre de zoom
      uirevision: false, // Désactiver l'avertissement "Double-tap to zoom back out"
    };

    if (loga==true){layout.yaxis.type = 'log'}
    
    Plotly.newPlot(plotDiv, [trace], layout, config);   
  }

  // this.http.get('/osur/display_map').subscribe((mapData: any) => {
  getMapData(): Observable<{datas: any[], layouts: any, plotlyConfig: any}> {

    return this.http.get('/osur/display_map').pipe(
      map((mapData: any) => {
        const couleurs = mapData['area'].map((v:any) => this.couleurService.attribuerCouleur(v, 200,'rouge','vert'));
        const tailles = mapData['area'].map((v:any) => v);
        const altitudes = Array.from({ length: mapData['id'].length }, (_, index) => index);
        //console.log(bretagne.features)
        const codepostal = Array.from({ length: bretagne.features.length }, (_, index) => bretagne.features[index].properties.code);
        const zs = Array.from({ length: bretagne.features.length }, (_, index) => index);
        //console.log(codepostal)


        const communes = [
          {
            type: 'choroplethmapbox',
            locations: codepostal, z: zs,
            featureidkey: 'properties.code',
            geojson: bretagne,
          }
        ];
        
        const datas = [
          {
            type: 'scattermapbox',
            lon: mapData['lo'],
            lat: mapData['la'],
            marker: {
              size: tailles,
              color: couleurs,

            },
            hoverinfo: 'text+event',
            text: mapData['name'],
            ids: mapData['id'],
            // z: altitudes
          }
        ];
        


        const layouts = {
          dragmode: 'zoom',
          mapbox: {
            style: 'open-street-map',
            layers: [
              {
                sourcetype: 'raster',
                // source: ["https://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}"]
              }
            ],
            center: { lat: 48.2141667, lon: -2.9424167 },
            zoom: 6.8
          },
          margin: { r: 0, t: 0, b: 0, l: 0 },
          showlegend: false,
          clickmode: 'event+select',
          displaylogo: false,
        };

        const plotlyConfig = { displayModeBar: false };
        // const ids = mapData['id']
        

        return {datas, layouts, plotlyConfig };
      })
    );
  }


  

  showGraphic(): Promise<any>{
		return lastValueFrom(this.http.get("osur/display_scatter_plot", { responseType: 'json' }));
  }

  dlMap(): Observable<any> {
      return this.http.get("osur/display_map", { responseType: 'text' });
  }

  dlMap2(): Promise<any>{
		return lastValueFrom(this.http.get("osur/display_map", { responseType: 'json' }));
  }

  setCurrentLoc(loc: string){
    this.currentLoc.next(loc);
  }

  getCurrentLoc(){
    return this.currentLoc.asObservable();
  }

  setCurrentID(id: string){
    this.currentID.next(id);
  }

  getCurrentID(){
    return this.currentID.asObservable();
  }

  getCurrentColor(){
    return this.couleur.asObservable();
  }

  setCurrentColor(c: string){
    this.couleur.next(c);
  }

  getSelected(){
    return this.selected.asObservable();
  }

  setSelected(p: any){
    if (typeof p === "undefined"){
      this.selected.next(false);
    }
    else{
      this.selected.next(true);
    }
    
  }

  filtrageDates(debut: number, fin: number, x: any[], y: any[]){
    const indices: number[] = [];
    let filtredX: string[] = [];
    let filtredY: number[] = [];

    x.forEach((item: string, index: number) => {
        const year = new Date(item).getFullYear();
        if (year >= debut && year <= fin) {
          console.log(year)
          filtredX.push(item);
          indices.push(index);
        }
    });
    indices.forEach((item:number, index:number) => {
      filtredY.push(y[item])
    })
    return [filtredX, filtredY]
  }

  
}