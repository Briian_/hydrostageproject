import { Injectable } from '@angular/core';
import type {Condition, Constraint} from '../model/rules';
import type {ParametersGroup} from '../model/parameters-group';

@Injectable({
  providedIn: 'root'
})
export class ParametersRulesService {

  private parametersRulesMap: Map<string, Constraint[]> = new Map<string, Constraint[]>();
  private disabledParams: Set<string> = new Set<string>();
  private parametersValues: Map<string, string> = new Map<string, string>();

  constructor() { }

  initializeMappingParamGroup(path: string, paramGroup: ParametersGroup, rules: Constraint[]): void {
    path += paramGroup.name[0] + '/';
    if (paramGroup.Parameter != undefined) { // base case
      for (let param of paramGroup.Parameter) {
        let pathParam : string = path + param.name;
        this.parametersValues.set(pathParam, param.value[0]);
        this.initializeMappingParam(pathParam, rules);
      }
    }

    if (paramGroup.ParametersGroup != undefined) {
      for (let subParamGroup of paramGroup.ParametersGroup) {
        this.initializeMappingParamGroup(path, subParamGroup, rules);
      }
    }
  }

  private initializeMappingParam(pathParam: string, rules: Constraint[]) : void {
    const associatedRules = rules.filter(rule => this.isParamInCondition(pathParam, rule.condition));
    this.parametersRulesMap.set(pathParam, associatedRules);
    for(let rule of associatedRules) {
      this.applyOrUnapplyRule(rule);
    }
  }

  private isParamInCondition(pathParam: string, condition: Condition[]): boolean {
    for (let cond of condition) {
      const pathCondParam = cond.parametersGroup + '/' + cond.parameterName;
      if(pathParam == pathCondParam) {
        console.log("FOUND A RULE !");
        return true;
      }
    }
    return false;
  }

  private applyOrUnapplyRule(rule: Constraint) {
    if(this.conditionsSatisfied(rule.condition)) {
      this.applyActions(rule);
    } else {
      this.unapplyActions(rule);
    }
  }

  private getRulesForParameter(pathParam: string): Constraint[] {
    return this.parametersRulesMap.get(pathParam) || [];
  }

  isParameterDisabled(pathParam: string): boolean {
    for (const disabledPath of this.disabledParams) {
      if ((disabledPath.endsWith('/*') && pathParam.startsWith(disabledPath.slice(0, -1))) || pathParam === disabledPath) {
        return true;
      }
    }
    return false;
  }

  updateValue(newValue: string, pathParam: string): void {
    this.parametersValues.set(pathParam, newValue);
    let relevantRules : Constraint[] = this.getRulesForParameter(pathParam);
    if(relevantRules.length != 0) {
      for (let rule of relevantRules) {
        this.applyOrUnapplyRule(rule);
      }
    }
  }

  private conditionsSatisfied(conditions: Condition[]): boolean {
    return conditions.every(condition => {
      let pathParam : string = condition.parametersGroup + '/' + condition.parameterName;
      const parameterValue = this.parametersValues.get(pathParam);
      return this.evaluateCondition(condition, parameterValue);
    });
  }

  private evaluateCondition(condition: Condition, parameterValue: any): boolean {
    switch (condition.logicalOperator[0]) {
      case 'equal':
        return parameterValue === condition.value[0];
      case 'different':
        return parameterValue !== condition.value[0];
      default:
        return false;
    }
  }

  private applyActions(rule: Constraint): void {
    rule.action.forEach(action => {
      let pathToDisable: string = action.parametersGroup + '/';
      pathToDisable += action.logicalOperator == "disableParameter" ? action.parameterName : '*';
      this.disabledParams.add(pathToDisable);
      console.log("disabling " + pathToDisable);
    });
  }

  private unapplyActions(rule: Constraint): void {
    rule.action.forEach(action => {
      let pathToEnable: string = action.parametersGroup + '/';
      pathToEnable += action.logicalOperator == "disableParameter" ? action.parameterName : '*';
      if(this.isParameterDisabled(pathToEnable)) {
        this.disabledParams.delete(pathToEnable);
        console.log("enabling " + pathToEnable);
      }
    });
  }
}
