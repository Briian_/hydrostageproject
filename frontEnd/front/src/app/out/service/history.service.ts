import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { lastValueFrom } from 'rxjs';
import {Filter, Simulation, SimulationComplete} from "../model/history";

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(private http: HttpClient) { }

  getHistory(filters: Filter[] = [], sort: string = 'date'): Promise<Simulation[]> {
    return lastValueFrom(this.http.post<Simulation[]>("osur/history", {
      filters: filters,
      sort: sort
    }));
  }

  getProperties(): Promise<string[]> {
    return lastValueFrom(this.http.get<string[]>("osur/history/properties"));
  }

  deleteSimulation(simulation: SimulationComplete): Promise<any> {
    return lastValueFrom(this.http.delete(`osur/history/${simulation.name}`));
  }

  setSimulationFavorite(simulation: SimulationComplete, favorite: boolean): Promise<any> {
    return lastValueFrom(this.http.put(`osur/history/${simulation.name}/${favorite}`, {}));
  }

  renameSimulation(simulation: SimulationComplete): Promise<any> {
    return lastValueFrom(this.http.put(`osur/history/${simulation.name}`, {
      "new_name": simulation.renameValue
    }));
  }

  getSimulationXml(simulation: SimulationComplete): Promise<any> {
    return lastValueFrom(this.http.get(`osur/history/${simulation.name}`, { responseType: 'text' }));
  }
}
