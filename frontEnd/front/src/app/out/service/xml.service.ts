import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class XmlService { // service used for http requests


	constructor(private http: HttpClient){}

	getXmlNames(): Promise<Array<string>>{ // request the list of xml available
		return lastValueFrom(this.http.get<Array<string>>("osur/getxmlnames"));
	}

	getXml(xmlName:String):Promise<string> { // request the xml with xmlName
		return lastValueFrom(this.http.get(`osur/getxml/${xmlName}`, { responseType: 'text' }));
  	}

	getXmlRules(xmlName:String):Promise<string> {
		return lastValueFrom(this.http.get("osur/get_rules/" + xmlName,{ responseType: 'text' }));
	}

	runCydre(xml:string): Promise<any>{ // send the xml to the api in backEnd
		return lastValueFrom(this.http.post("osur/run_cydre", xml, { responseType: 'text' }));
  	}

	runCydre3(xml:string): XMLHttpRequest{ // send the xml to the api in backEnd
		let xhr = new XMLHttpRequest();
		xhr.open("POST", "osur/run_cydre", true);
		xhr.setRequestHeader("Content-Type", "text/xml");
		xhr.send(xml);

		return xhr;

		//return this.http.post("osur/run_cydre", xml, { responseType: 'text' , observe: 'response'});
	}

	runCydre2(): Observable<any>{ // run the cydre app (with the local cydre project)
		return this.http.get("osur/run_cydre");
  	}
}
