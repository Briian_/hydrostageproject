export class Data<T> {

  public static nextColorIndex: number = 0;
  private static predefinedColors = [
    '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd',
    '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'
  ];
  static colorMap: Map<string, string> = new Map<string, string>();

  label!: string;
  data!: T[];
  fill!: boolean;
  private _color: string;
  tension!: number;
  hidden!: boolean;

  constructor(
    label: string,
    data: T[],
    fill: boolean = false,
    tension: number = 0.4,
    hidden: boolean = false
  ) {
    
    let labelDecoded;
    try{
        // If the string is UTF-8, this will work and not throw an error.
      labelDecoded=decodeURIComponent(escape(label));
    }catch(e){
        // If it isn't, an error will be thrown, and we can assume that we have an ISO string.
        labelDecoded=label;
    }
    this.label = labelDecoded;
    this.fill = fill;
    this.tension = tension;
    this.data = data;
    this.hidden = hidden;

    const existingColor = Data.colorMap.get(label);
    this._color = existingColor ? existingColor : this.getNextColor();

  }

  private getNextColor(): string {
    const index = (Data.nextColorIndex++) % Data.predefinedColors.length;
    Data.colorMap.set(this.label, Data.predefinedColors[index]);
    return Data.predefinedColors[index];
  }

  get color(): string {
    return this._color;
  }

  set color(newColor: string) {
    this._color = newColor;
    Data.colorMap.set(this.label, newColor);
  }

}


