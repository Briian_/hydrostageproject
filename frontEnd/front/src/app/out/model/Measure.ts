class Measure {
  t: Date;
  Q: string;

  constructor(t: string, Q: string) {
    this.t = new Date(t);
    this.Q = Q;
  }
}

export default Measure;
