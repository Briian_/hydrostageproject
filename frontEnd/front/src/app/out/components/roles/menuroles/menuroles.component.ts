import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/out/service/data.service';
import { XmlService } from 'src/app/out/service/xml.service';

@Component({
  selector: 'app-menuroles',
  templateUrl: './menuroles.component.html',
  styleUrls: ['./menuroles.component.scss']
})
export class MenuRolesComponent implements AfterViewInit {

  protected xmlNames: Array<string>;

  public constructor(private requestService: XmlService, private data: DataService, private router: Router){
    this.xmlNames = [];
  }

  public ngAfterViewInit(): void {
     // we request all xml names
    this.requestService.getXmlNames()
    .then(names => {
      this.xmlNames = names;
    })
    .catch(err => {
      console.log(err);
    })
  }

  get selectedRole(){
    return this.data.userRole;
  }

  set selectedRole(role:string){
    this.data.userRole=role;
  }

  protected async sendXmlName(){
    const e = document.getElementById("xml-select") as HTMLSelectElement;
    // we change the path of the request for the xml
    this.data.xmlPath = e.options[e.selectedIndex].text;

    await this.data.requestXml();
    await this.data.getJSONData() // load the xml parsed into JSON in parameters
    .then(response=>{}).catch(error=>{});

    this.router.navigateByUrl("/xmlViewRole");
  }
}
