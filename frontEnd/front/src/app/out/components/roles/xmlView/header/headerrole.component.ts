import { Component } from '@angular/core';


interface Role {
  name: string;
  code: string;
}

@Component({
  selector: 'app-headerrole',
  templateUrl: './headerrole.component.html',
  styleUrls: ['./headerrole.component.scss']
})
export class HeaderRoleComponent {

  roles: Role[] | undefined;

  selectedRole: Role | undefined;

  ngOnInit() {
    this.roles = [
      { name: 'Observer', code: 'observer' },
      { name: 'Scientist', code: 'scientist' },
      { name: 'Admin', code: 'admin' },
    ];
  }

}
