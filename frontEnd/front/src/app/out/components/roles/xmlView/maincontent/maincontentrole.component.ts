import { Component } from '@angular/core';


interface Role {
  name: string;
  code: string;
}
@Component({
  selector: 'app-maincontentrole',
  templateUrl: './maincontentrole.component.html',
  styleUrls: ['./maincontentrole.component.scss']
})
export class MaincontentRoleComponent {

  roles: Role[] | undefined;

  selectedRole: Role | undefined;

  ngOnInit() {
      this.roles = [
          { name: 'Observer', code: 'observer' },
          { name: 'Scientist', code: 'scientist' },
          { name: 'Admin', code: 'admin' },
      ];
  }


  toParameter(){
    console.log("toParameter");
  }
}
