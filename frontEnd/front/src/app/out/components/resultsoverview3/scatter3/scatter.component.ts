import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from 'src/app/service/data.service';
import { XmlService } from 'src/app/service/xml.service';

import { PlotlyService } from 'src/app/service/plotly.service'

@Component({
  selector: 'app-scatter3',
  templateUrl: './scatter.component.html',
  styleUrls: ['./scatter.component.scss']
})
export class Scatter3Component {
  startYear: number = 0;
  endYear: number = 0;
  plot: any;
  plotX: string[] = [];
  plotY: number[] = [];
  loisance: boolean = false
  title: string = "Évolution des débits sur le cours d'eau du Nançon mesurée dans la commune de Lécousse"
  plotXfiltered: any = []
  plotYfiltered: any = []

  loga: string = "Logarithmique"
  loga_bool: boolean = false

  constructor(private plotlyService: PlotlyService, private xmlService: XmlService) {}

  ngOnInit(): void {

    this.startYear = 1990;
    this.endYear = 2020;

    this.afficheGraphique();

  }

  afficheGraphique() {
    this.plotlyService.showGraphic().then(
      (data: any) =>{
        this.plotX = data['x'];
        this.plotY = data['y'];

        this.plotX = this.plotX.map((date: string) => {
          return new Date(date).toISOString();
        });

        this.plotlyService.plotScatter(this.title,"plot",this.plotX,this.plotY,this.loga_bool);
      },
      error => {
        console.error('Error:',error);
      }
    );
  }

  majIntervalle() {
    let xy = this.plotlyService.filtrageDates(this.startYear, this.endYear, this.plotX, this.plotY);
    this.plotlyService.plotScatter(this.title,"plot",xy[0],xy[1],this.loga_bool);
  }

  resetIntervalle(){
    this.plotlyService.plotScatter(this.title,"plot",this.plotX,this.plotY,this.loga_bool);
  }

  logarithmique(){
    this.loga_bool = !this.loga_bool
    this.plotlyService.plotScatter(this.title,"plot",this.plotX,this.plotY,this.loga_bool);
    // if (this.loga_bool){this.loga = "Logarithmique"} else {this.loga = "Linéaire"}
  }
}
