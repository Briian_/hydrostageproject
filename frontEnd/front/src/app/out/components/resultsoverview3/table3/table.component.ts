import { Component } from '@angular/core';
import { PlotlyService } from 'src/app/service/plotly.service';
import { TableService } from 'src/app/service/table.service';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CdkTableDataSourceInput } from '@angular/cdk/table';
import { CouleurService } from 'src/app/service/couleur.service';


@Component({
  selector: 'app-table3',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class Table3Component {
  startYear: number = 0;
  endYear: number = 0;
  loisance: boolean = true

  dates: string[] = [];
  values: any = {};
  locCourant: string = "";
  idCourant: string = "";
  couleur: string = "";

  selected: boolean = false;

  displayedColumns: string[] = [];

  // Create a MatTableDataSource
  dataSource: CdkTableDataSourceInput<any> = [];

  constructor(public tableService: TableService, private plotlyService: PlotlyService, private colorService: CouleurService){}

  ngOnInit(){
    this.plotlyService.getCurrentID().subscribe(val=>{
      this.idCourant = val;
    })
    this.plotlyService.getCurrentColor().subscribe(val=>{
      this.couleur = val;
    })
    this.plotlyService.getCurrentLoc().subscribe(val => {
      this.locCourant = "Corrélation de la recharge de nappe de la Loisance en 2023 avec celle de "+val;
    })
    this.plotlyService.getSelected().subscribe(val =>{
      if (val) {
        this.selected = true;
        this.afficheGraphiqueUnique();
      }
      else {
        this.selected = false;
      }
    })
  }


  afficheGraphiqueUnique() {
    this.tableService.generateTable().then(
      (data: any) =>{
        this.dates = data['x'];
        this.values = data['y'];

        this.dates = this.dates.map((date: string) => {
          return new Date(date).getFullYear().toString();
        });
        if (this.selected){
          this.tableService.plotTableUnique(this.locCourant,"table",this.dates,this.values[this.idCourant],this.idCourant,this.couleur);
          // this.tableService.afficherTableau(this.idCourant,"tableData", this.dates, this.values[this.idCourant])
          this.startYear = this.parseToInt(this.dates[0]);
          this.endYear = this.parseToInt(this.dates[this.dates.length-1]);
        }
      },
      error => {
        console.error('Error:',error);
      }
    );
  }

  // afficheGraphique() {
  //   this.tableService.generateTable().then(
  //     (data: any) =>{
  //       this.dates = data['x'];
  //       this.values = data['y'];

  //       this.dates = this.dates.map((date: string) => {
  //         return new Date(date).toISOString();
  //       });

  //       this.tableService.plotTable("Table","table",this.dates,this.values);
  //     },
  //     error => {
  //       console.error('Error:',error);
  //     }
  //   );
  // }

  majIntervalle() {
    let xy = this.plotlyService.filtrageDates(this.startYear, this.endYear, this.dates, this.values[this.idCourant])
    this.tableService.plotTableUnique(this.locCourant + " entre " + this.startYear.toString() + " et " + this.endYear.toString() ,"table",xy[0], xy[1], this.idCourant,this.couleur);
  }

  resetIntervalle(){
    this.tableService.plotTableUnique(this.locCourant,"table",this.dates,this.values[this.idCourant], this.idCourant,this.couleur);
  }

  // getKey(d: any){
  //   return Object.keys(d);
  // }

  // getValues(d: any){
  //   return Object.values(d);
  // }
  // getValuesWithKey(d: any, k:any){
  //   return d[k];
  // }

  // indexation(d: any, i: number){
  //   return d[i];
  // }

  parseToInt(v: any){
    return parseInt(v.toString());
  }

  attribuerCouleur(value: any, maxi: number, couleur1: string, couleur2: string): any {
    // if (value < 0.5) {
    //   return { color: 'red' };
    // } else if (value > 0.5) {
    //   return { color: 'green' };
    // }

    // return {color: 'rgb(0,'+ Math.abs(value)*255+',0)'};

    return {'background-color' : this.colorService.attribuerCouleur(value, maxi, couleur1, couleur2)};
  }

  attribuerCouleur2(value: number){
    let factor, c1, c2 = 0;
    value = Math.abs(value)
    if (value < 1/3){
      factor = value*3;
      c1 = 255;
      c2 = 255 * factor;
    }
    else if(value >= 1/3 && value < 2/3){
      factor = (value - .3);
      c1 = (1-factor) * 255;
      c2 = 255;
    }
    else{
      factor = value;
      c1 = (1-factor) * 255;
      c2 = 255;
    }

    return {'background-color' : 'rgb('+c1+','+c2+',0)'}
  }
}
