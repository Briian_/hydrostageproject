import { HttpClient } from '@angular/common/http';
import { Component, NgModule } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
declare let Plotly: any;
import * as mapboxgl from 'mapbox-gl';
import { PlotlyService } from 'src/app/service/plotly.service';
import { XmlService } from 'src/app/service/xml.service';

@Component({
  selector: 'app-map3',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class Map3Component {
  names: String[] = [];
  longitudes: Float64Array = new Float64Array();
  latitudes: Float64Array = new Float64Array();
  carte: any;
  myPlot: any;
  datas: any;
  polygons: any;
  layouts: any;
  style: any;
  plotlyConfig: any;

  selectedNappe: any

  nappes : any;

  // ids: any;

  constructor(private http: HttpClient ,private xmlService: XmlService, private plotlyService: PlotlyService){}

  ngOnInit(): void {
    this.initialiserCarte();
  }

  setPointInfoToService(loc: any, id: any, sel: any, col: any){
    this.plotlyService.setCurrentLoc(loc);
    this.plotlyService.setCurrentID(id)
    this.plotlyService.setSelected(sel)
    this.plotlyService.setCurrentColor(col)
    console.log(loc)
    console.log(id)
    console.log(sel)
    console.log(col)
  }

  onCityClick(event: any) {
    console.log(event)
    console.log('Point cliqué :', event.points[0].text);
    this.setPointInfoToService(event.points[0].text,event.points[0].id,event.points[0].fullData.selectedpoints,event.points[0]["marker.color"]);

    this.selectedNappe = event.points[0].text
    console.log(event.points[0].id)
    console.log(event.points[0].fullData.selectedpoints)
  }

  // actualiserCarte(){
  //   this.plotlyService.dlMap().subscribe((htmlContent) => {
  //     this.carte = this.sanitizer.bypassSecurityTrustHtml(htmlContent);
  //   });
  // }

  // actualiserCarte2(){
  //   this.plotlyService.dlMap2().then(
  //     (data: any) =>{
  //       this.datas = data;
  //       this.names = data['name'];
  //       this.longitudes = data['lo'];
  //       this.latitudes = data['la'];

  //       this.plotMap("Show Map","map",this.names,this.longitudes,this.latitudes);
  //       console.log(this.longitudes)
  //     },
  //     error => {
  //       console.error('Error:',error);
  //     }
  //   );
  // }



  // plotMap(title: string, plotDiv: string, names: String[], longitudes: Float64Array, latitudes: Float64Array){

  //   const data = [{
  //       type: 'scattermapbox',
  //       lon: longitudes,
  //       lat: latitudes,
  //       marker: {color: 'fuchsia', size: 8},
  //       hoverinfo: 'text+event',
  //       text: names,
  //   }];

  //   const layout = {
  //     dragmode: 'zoom',
  //     mapbox: {
  //       style: 'open-street-map',
  //       layers: [
  //         {
  //           "sourcetype": "raster",
  //         },
  //         {
  //           sourcetype: "raster",
  //         }
  //       ],
  //       center: {lat: 48.2141667, lon: -2.9424167}, zoom: 6.8},
  //       margin: {r: 0, t: 0, b: 0, l: 0},
  //       showlegend: false,
  //       clickmode: 'event+select'

  //     };

  //   Plotly.newPlot(plotDiv, data, layout);
  // }

  initialiserCarte(){
    this.plotlyService.getMapData().subscribe(data => {
      this.nappes = data.datas[0].text
      this.datas = data.datas;
      //this.polygons = data.polygon;
      this.layouts = data.layouts;
      this.style = this.layouts.mapbox.style;
      this.plotlyConfig = data.plotlyConfig;
    });
  }

  changeMapStyle(style: string){
    this.style = style;
    let center = this.layouts.mapbox.center;
    let zoom = this.layouts.mapbox.zoom;

    this.plotlyService.getMapData().subscribe(data => {
      this.layouts = data.layouts;
      this.layouts.mapbox.style = this.style;
      this.layouts.mapbox.center = center;
      this.layouts.mapbox.zoom = zoom;

    })

  }

  actualiserNappeRechercher(){
    let indice = this.datas[0].text.indexOf(this.selectedNappe)
    this.setPointInfoToService(this.datas[0].text[indice], this.datas[0].ids[indice],[indice],this.datas[0].marker.color[indice])

    this.plotlyService.getMapData().subscribe(data => {
      this.datas[0].selectedpoints = [indice]
      this.layouts = data.layouts;
      this.layouts.mapbox.style = this.style;
      this.layouts.mapbox.center = { lat: this.datas[0].lat[indice], lon: this.datas[0].lon[indice] };
      this.layouts.mapbox.zoom = 10;


    })
    console.log(this.datas[0].text.indexOf(this.selectedNappe))

  }

  afficheNom(event: any){
    console.log(event.text)
  }

}