import { Component, OnInit } from '@angular/core';
import { XmlService } from 'src/app/service/xml.service';

@Component({
  selector: 'app-result3',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class Result3Component implements OnInit{ // manage the results display

  result: any;

  constructor(private service:XmlService){}

ngOnInit(): void {
  this.result = true;
  // this.service.runCydre1().then(
  //   response =>{
  //   this.result = response;
  // },
  // error => {
  //   console.error('Error:',error);
  // }
  // );
}

}

