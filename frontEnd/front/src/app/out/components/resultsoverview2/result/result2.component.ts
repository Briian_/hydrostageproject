import { Component, OnInit } from '@angular/core';
import { XmlService } from 'src/app/out/service/xml.service';
import * as Plotly from 'angular-plotly.js';

@Component({
  selector: 'app-result2',
  templateUrl: './result2.component.html',
  styleUrls: ['./result2.component.scss']
})
export class Result2Component implements OnInit{ // manage the results display
  receivedData: string[] = [];
  reset: boolean = false;

  result: any;
  isSidebarClosed = false;


  constructor(private service:XmlService){}

ngOnInit(): void {
  this.service.runCydre(XmlService.name).then(
    response =>{
    this.result = response;
  },
  error => {
    console.error('Error:',error);
  }
  );
}

toggleSidebar() {
    this.isSidebarClosed = !this.isSidebarClosed;
    const resizeEvent = new Event('resize');
    window.dispatchEvent(resizeEvent);
}
  receiveData(data: string[]) {
    this.receivedData = data;
  }
  receiveReset(reset: boolean) {
    this.reset = reset;
  }
}


