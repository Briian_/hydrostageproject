import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Data } from 'src/app/out/model/Data';
import { Chart, ChartData,ChartDataset, ChartOptions } from 'chart.js';
import { DataService } from 'src/app/out/service/data.service';

import zoomPlugin from 'chartjs-plugin-zoom';

import Measure from 'src/app/out/model/Measure';
import MeasurementPoint from 'src/app/out/model/MeasurementPoint';

interface LegendItem{
  label: string;
  color: string;
  hidden: boolean;
}

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})

export class ChartComponent implements OnInit ,OnChanges {


  // cacheDataset contains all the data of all locations and all dates. ShownDataset contains only the data of all locations and for selected dates.
  shownDataset: Data<Measure>[] = [];
  cacheDataset: Data<Measure>[] = [];

  // measurePoints contains all the locations
  @Input() measurePoints: MeasurementPoint[] = [];

  // chart title
  title: string = "";

  private mutationObserver: MutationObserver | null = null;

  // date selector variables
  dateStart: Date = new Date("2021-01-04");
  dateEnd: Date = new Date("2022-02-24"); //suggestion : mettre la date du jour
  errorDate : boolean = false;

  // chart parameters
  options: ChartOptions = {};
  data : ChartData = {datasets: []};

  // simple/superpostion mode selector
  mode_sup : boolean = false;

  // logarithmic/linear mode selector
  scale_log: boolean = false;

  // init legend
  legendsItems : LegendItem[] = [];

  // export menu
  items = [
    {
      label: 'CSV',
      icon: 'pi pi-file',
      command: () => {
        let file = this.generateCsv();
        this.makeDownload(file, "data.csv");
      }
    },
    {
      label: 'PNG',
      icon: 'pi pi-chart-line',
      command: () => {
        this.generatePng().then((file: Blob) => {;
          this.makeDownload(file, "data.png");
        });
      }
    },
  ];

  // period selector possibilities
  periods = ["Année", "Mois"];
  period = "Année";

  constructor(private dataService: DataService) {
  }


  /**
   * Init the chart.
   */
  ngOnInit() {
    this.initData()
  }


  /**
   * Update the chart when the measurePoints input change.
   *
   * @param changes - changes of the input
   */
  ngOnChanges(changes: SimpleChanges) {
    if(changes['measurePoints'].previousValue == undefined){
      this.initData();
    }else{
      const previousLocations = (changes['measurePoints'].previousValue || []) as MeasurementPoint[];
      const currentLocations = (changes['measurePoints'].currentValue || []) as MeasurementPoint[];

      const addedLocations = currentLocations.filter(location => !previousLocations.some(prevLocation => prevLocation.ID === location.ID));
      const removedLocations = previousLocations.filter(prevLocation => !currentLocations.some(location => location.ID === prevLocation.ID));

      addedLocations.forEach(location => {
        this.addLocation(location).then(() => this.updateData());
      });

      removedLocations.forEach(location => {
        this.removeLocation(location.name);
        this.updateData();
      });
    }
    this.updateTitle();
  }

  /**
   * Update the chart title.
   */
  updateTitle(){
    if(this.measurePoints.length > 0){
      this.title = "Débits à ";
      for(let i = 0; i < this.measurePoints.length; i++){
        if(i != 0){
          this.title += ", ";
        }
        this.title += this.decode(this.measurePoints[i].name);
      }
    }else{
      this.title = "";
    }
  }


  /**
   * Update data and options of the chart.
   */
  updateChart() {
    this.updateData();
    this.updateOptions();
  }

  /**
   * Update data of the chart.
   *
   * @param labels - labels to set, if not given the labels are generated from the first dataset
   *
   */
  updateData(labels : string[] | undefined = undefined){
    let datasets = []
    for(let i = 0; i < this.shownDataset.length; i++){
        let dataset : ChartDataset = {
          label: this.shownDataset[i].label,
          data: this.shownDataset[i].data.map((measure: Measure) => {
              return Number(measure.Q);
              }),
          fill: false,
          pointRadius: 0,
          borderWidth: 1,
          backgroundColor: this.shownDataset[i].color,
          borderColor: this.shownDataset[i].color,
          pointHoverRadius: 5,
          pointHoverBorderWidth: 2,
          pointHitRadius: 10,
          hidden: this.shownDataset[i].hidden,
        };

        datasets.push(dataset);
    }

    let generatedLabels : any[]= [];
    if(labels != undefined){
      generatedLabels = labels;
    }else{
    if(this.shownDataset.length != 0){
      generatedLabels = this.shownDataset[0].data.map((measure: Measure) => {
        return measure.t;
      });
    }
    }

    this.data = {
      labels: generatedLabels,
      datasets: datasets,
    };
  }

  /**
   * Update options of the chart.
   *
   * @param displayLegend - display the legend or not
   * @param min - min value of the x axis
   * @param max - max value of the x axis
   *
   * @returns void
   */
  updateOptions(displayLegend : boolean = false, min : string | number | undefined = undefined, max : string | number| undefined = undefined){
    this.options = {
      animation: false,
      scales: {
        x:{
          title: {
            display: true,
            text: 'Date'
          },
          min: min,
          max: max,
        },
        y: {
          title: {
            display: true,
            text: 'Débit (m3/s)'
          },
          type: this.scale_log ? 'logarithmic' : 'linear',
        },
      },
      plugins: {
        legend: {
            display: displayLegend,
            position :'right',
        },
        zoom: {
          zoom: {
            wheel: {
              enabled: true,
            },
            drag: {
              enabled: true,
            },
            pinch: {
              enabled: true
            },
            mode: 'x',
          }
        }
    }
    };
    Chart.register(zoomPlugin);
  }

  /**
   * Update the color of a location (dataset link to this location).
   *
   * @param event - event of the color picker
   * @param location - location to update
   * @param c - color to set
   */
  updateColor(event : any,location : any, c : any){
    let measurementPoint = location as string;
    let color = c as string;
    for(let dataset of this.shownDataset){
      if(dataset.label === measurementPoint){
        dataset.color = color;
      }
    }
    this.updateData();
  }




  /**
   * Init data of the chart.
   * Add all locations in cacheDataset.
   */
  initData(){
    for(let location of this.measurePoints){
      this.addLocation(location).then(() => {
      });
    }
  }

  /**
   * Add a location in cacheDataset.
   *
   * @param location - location to add
   *
   * @returns Promise<void>
   */
  addLocation(location: MeasurementPoint ): Promise<void> {
    return new Promise<void>((resolve) => {
      this.dataService.getMesurementData(location.ID).then((data: any) => {
        this.cacheDataset.push(new Data(this.decode(location.name), data, false, 0.4));
        this.updatePeriod();
        resolve();
      });
    });
  }

  /**
   * Remove a location from cacheDataset and shownDataset.
   *
   * @param location - location to remove
   *
   */
  removeLocation(location: string|undefined) {
    this.measurePoints = this.measurePoints.filter((loc: MeasurementPoint) => {
      return loc.ID !== location;
    });
    this.cacheDataset = this.cacheDataset.filter((dataset: Data<Measure>) => {
      return dataset.label != location;
    });
    this.shownDataset = this.shownDataset.filter((dataset: Data<Measure>) => {
      return dataset.label != location;
    });
    if(this.measurePoints.length ==0 && this.mode_sup){
      this.mode_sup = false;
    }
    this.updateData();
  }

  /**
   * Hide a location from the chart without removing it from showDataset.
   *
   * @param location - location to hide
   */
  hideLocation(location:string |undefined) {
    for(let dataset of this.shownDataset){
      if(dataset.label === location){
        dataset.hidden = !dataset.hidden;
      }
    }
    this.updateData();
  }


  /**
   * Update shownDataset with data between dateStart and dateEnd.
   */
  updatePeriod() {
    this.shownDataset.splice(0, this.shownDataset.length);;
    this.cacheDataset.forEach((dataset: Data<Measure>) => {
      let values = dataset.data
        .filter((measure: Measure) => {
          return new Date(measure.t) >= this.dateStart && new Date(measure.t) <= this.dateEnd;
        })

      this.shownDataset.push(new Data(dataset.label, values, false, 0.4));
    });
    this.updateChart();
  }

  /*
  * Display start date in red if after stop datae.
  */

  checkIfPossibleDate(): any {
    // Compare the left and right dates
    const isStartDateGreaterThanStop = this.dateStart > this.dateEnd;
    this.errorDate = isStartDateGreaterThanStop;

    // Return a style object based on the comparison
    return {
      'color': isStartDateGreaterThanStop ? 'red' : 'inherit',
    };
  }

  /**
   * Create superposition of data in shownDataset
   * Data are splited by period (defined by the period global variable)
   */
  updateSuperposition() {
    // TODO voir si il faut reset le zoom ou non
    this.shownDataset.splice(0, this.shownDataset.length);

    // change dateStart to the first day of the year or month to avoid problems with superposition
    if(this.period === "Année"){
      this.dateStart = new Date(this.dateStart.getFullYear().toString());
    }else if(this.period === "Mois"){
      let month : string = (this.dateStart.getMonth()+1) > 9 ? (this.dateStart.getMonth()+1).toString(): "0"+(this.dateStart.getMonth()+1).toString();
      this.dateStart = new Date(this.dateStart.getFullYear().toString() + "-" + month);
    }

    let datasets = this.cacheDataset.filter((dataset: Data<Measure>) => {
      return dataset.label === this.decode(this.measurePoints[0].name);
    });
    let values = datasets[0].data
      .filter((measure: Measure) => {
        return new Date(measure.t) >= this.dateStart && new Date(measure.t) <= this.dateEnd;
      })

    //copy splited data in shownDataset
    let valuesByPeriod = this.splitByPeriod(values);
    for (let i = 0; i < valuesByPeriod.length; i++) {
      this.shownDataset.push(new Data(valuesByPeriod[i].label, valuesByPeriod[i].data, false, 0.4));
    }

    let labels : any[]= [];
    if(this.period === "Année"){
      valuesByPeriod[0].data.forEach((measure: Measure) => {
        let tempDate : string = new Date(measure.t).toLocaleDateString("FR-fr");
        labels.push(tempDate.split("/")[1]+"-"+tempDate.split("/")[0]);
      })
    }else if(this.period === "Mois"){
      for(let i = 0; i < 31; i++){
        labels.push(i+1);
      }
    }
    this.updateData(labels);
    this.updateOptions();
  }

  /**
   * Split the given measures by period (defined by the period global variable).
   *
   * @param measures - measures to split
   *
   * @returns Data<Measure>[] - splited measures
   */
  splitByPeriod(measures: Measure[]): Data<Measure>[] {
    let result: Data<Measure>[] = [];
    let currentDate: Date = new Date(measures[0].t);
    let currentYear: number = currentDate.getFullYear();
    let currentMonth: number = currentDate.getMonth();

    // init the first period
    let currentPeriod !: Data<Measure>;
    if(this.period === "Année"){
      currentPeriod = new Data(currentYear.toString(), [], false, 0.4);
    }else if(this.period === "Mois"){
      let month: string = currentDate.toLocaleString('fr-FR', {month: 'long',})
      month = month.charAt(0).toUpperCase() + month.slice(1);
      currentPeriod = new Data(month+" "+currentDate.getFullYear(), [], false, 0.4);
    }

    // split the measures in periods depending on the period global variable
    for (let i = 0; i < measures.length; i++) {
      currentDate = new Date(measures[i].t);
      if (this.period === "Année") {
        if (currentDate.getFullYear() != currentYear) {
          result.push(currentPeriod);
          currentPeriod = new Data(currentDate.getFullYear().toString(), [], false, 0.4);
          currentYear = currentDate.getFullYear();
        }
      } else if (this.period === "Mois") {
        if (currentDate.getMonth() != currentMonth) {
          result.push(currentPeriod);
          let month: string = currentDate.toLocaleString('fr-FR', {month: 'long',})
          month = month.charAt(0).toUpperCase() + month.slice(1);
          currentPeriod = new Data(month+" "+currentDate.getFullYear(), [], false, 0.4);
          currentMonth = currentDate.getMonth();
        }
      }
      currentPeriod.data.push(measures[i]);
    }
    result.push(currentPeriod);
    return result;
  }

  /**
   * Change the mode of the chart (simple/superposition) and update the chart.
   */
  switchMode() {
    if (!this.mode_sup ) {
      this.updatePeriod();
      this.updateData();
    } else {
      this.updateSuperposition();
    }


    // console.log(this.mode);
  }


  /**
   * Generate a csv file from shownDataset.
   *
   * @returns Blob - csv file
   */
  generateCsv(): Blob {
    let csvData = "id,t,Q\n";
    this.shownDataset.forEach((dataset: Data<Measure>) => {
      dataset.data.forEach((value: Measure) => {
        csvData += `${dataset.label},${value.t},${value.Q}\n`;
      });
    });
    return new Blob([csvData], { type: 'text/csv' });
  }


  /**
   * Generate a png file from the chart.
   *
   * @returns Promise<Blob> - png file
   */
  generatePng(): Promise<Blob> {
    return new Promise<Blob>((resolve) => {
      let savedMin : string | number |undefined = undefined;
      let savedMax : string | number |undefined = undefined;
      if(this.options.scales != undefined ){
        savedMin = this.options.scales['x']?.min;
        savedMax = this.options.scales['x']?.max;
      }
      this.updateOptions(true,savedMin,savedMax);

      // Observer les changements dans le canvas du graphique
      this.observeChartUpdate(() => {
        let canvas = document.getElementsByTagName("canvas")[0];
        let data = canvas.toDataURL("image/png");

        this.updateOptions(false,savedMin,savedMax);

        let base64Data = data.replace(/^data:image\/(png|jpg);base64,/, '');
        let byteCharacters = atob(base64Data);
        let byteNumbers = new Array(byteCharacters.length);

        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        let byteArray = new Uint8Array(byteNumbers);
        let blob = new Blob([byteArray], { type: 'image/png' });

        this.disconnectMutationObserver();

        resolve(blob);
      });
    });
  }

  /**
   * Observe changes in the chart canvas.
   *
   * @param callback - function to call when a change is detected
   */
  private observeChartUpdate(callback: () => void) {
    const canvas = document.getElementsByTagName("canvas")[0];

    this.mutationObserver = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.target === canvas) {
          callback();
        }
      });
    });

    this.mutationObserver.observe(canvas, { subtree: true, attributes: true });
  }

  /**
   * Disconnect the mutation observer.
   */
  private disconnectMutationObserver() {
    if (this.mutationObserver) {
      this.mutationObserver.disconnect();
      this.mutationObserver = null;
    }
  }

  /**
   * Make the user download a file.
   *
   * @param file - file to download
   * @param name - name of the file
   *
   */
  makeDownload(file: Blob, name: string) {
    let link = document.createElement("a");
    link.href = URL.createObjectURL(file);
    link.download = name;
    link.click();
  }

  /**
   * Decode a label.
   *
   * @param labelToDecode - label to decode
   */
  decode(labelToDecode : string){
    try {
      return decodeURIComponent(escape(labelToDecode));
    }catch(e){
      return labelToDecode;
    }
  }
}
