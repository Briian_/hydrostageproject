import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/out/service/data.service';
import { XmlService } from 'src/app/out/service/xml.service';

@Component({
  selector: 'app-menuoverview',
  templateUrl: './menuoverview.component.html',
  styleUrls: ['./menuoverview.component.scss']
})
export class MenuOverviewComponent implements AfterViewInit {

  protected xmlNames: Array<string>;

  public constructor(private requestService: XmlService, private data: DataService, private router: Router){
    this.xmlNames = [];
  }

  public ngAfterViewInit(): void {
     // we request all xml names
    this.requestService.getXmlNames()
    .then(names => {
      this.xmlNames = names;
    })
    .catch(err => {
      console.log(err);
    })
  }

  protected sendXmlName(){
    const e = document.getElementById("xml-select") as HTMLSelectElement;
    // we change the path of the request for the xml
    this.data.xmlPath = e.options[e.selectedIndex].text;
    this.router.navigateByUrl("/xmlView");
  }
}
