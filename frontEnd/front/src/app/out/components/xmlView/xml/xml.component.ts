import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { interactoTreeUndoProviders } from 'interacto-angular';
import { ParametersGroup } from 'src/app/out/model/parameters-group';
import type {Rules} from 'src/app/out/model/rules';
import { DataService } from 'src/app/out/service/data.service';
import {ParametersRulesService} from 'src/app/out/service/parameters-rules.service';
import { XmlService } from 'src/app/out/service/xml.service';


@Component({
  selector: 'app-xml',
  templateUrl: './xml.component.html',
  styleUrls: ['./xml.component.scss'],
  providers: [interactoTreeUndoProviders()]
})
export class XmlComponent implements AfterViewInit {

  parameters: ParametersGroup;
  rules: Rules;

  constructor(private service: DataService,private requestService: XmlService,
    private parametersRulesService: ParametersRulesService, private router:Router){
    this.parameters = {name:[""]};
    this.rules = {constraint:[]};
  }

  ngAfterViewInit(): void {
    // if the user chose an xml, request the xml
    if (this.service.xmlPath !="--Please choose an xml--" && this.service.xmlPath !=""){
      this.service.requestXml();
    }
  }

  parseIntoJson(){
    if (this.service.xmlPath !== "--Please choose an xml--" && this.service.xmlPath !=""){
      this.service.getJSONData() // load the xml parsed into JSON in parameters
      .then(data => {
        this.parameters = data;
      });

      this.service.getJSONRules()
        .then(rules => {
          this.rules = rules;
          this.initializeMapping();
        })
        .catch(error => {
          console.error('Error fetching rules:', error);
        });
    }
  }


  initializeMapping() {
    // Ensure that both parameters and rules are available before initializing mapping
    if (this.parameters && this.rules && this.parametersRulesService) {
      this.parametersRulesService.initializeMappingParamGroup("", this.parameters, this.rules.constraint);
    }
  }


  convertFormat(input: any): any { //convert the format of the JSON to parse back into XML
    const attributes = ["name"];
    if (typeof input === "object") {
      const attributePairs: { [key: string]: string } = {};
      const result: any = {};

      for (const [key, value] of Object.entries(input)) { //go through all the elements
        if (attributes.includes(key)) {
          attributePairs[key] = value as string;
        } else if (Array.isArray(value)) {
          const newArray = value.map(item => this.convertFormat(item));
          result[key] = newArray;
        } else if (typeof value === "object") {
          result[key] = this.convertFormat(value);
        } else {
          result[key] = value;
        }
      }

      if (Object.keys(attributePairs).length > 0) {
        if (result["$"]) {
          Object.assign(result["$"], attributePairs);
        } else {
          result["$"] = attributePairs;
        }
      }

      return result;
    }
    return input;
  }



  runCydre(){
    //this.requestService.runCydre(this.service.jsonToXml(this.convertFormat(this.parameters))); // send modified xml (use api in backEnd)
    this.router.navigateByUrl("/result"); // go to result to launch the app (use with cydre project)
  }

}
