# -*- coding: utf-8 -*-
"""
Created on Mon Jul 17 15:35:15 2023

@author: nicol
"""

import time
import flask, random, xmltodict, json, os
from flask_cors import CORS, cross_origin
from flask import render_template
import pandas as pd
import plotly.express as px
import plotly.graph_objs as go
import plotly.io as pio
from datetime import datetime
from history_processing import (Simulation, filter_xml, find_value, get_available_properties, HISTORY_FOLDER,
                                delete_simulation, rename_simulation, set_favorite, unset_favorite, get_csv, get_xml,
                                get_date, MissingSimulationError)
import random
from datetime import datetime, timedelta

import csv


# --- Chargement des données ---

# Exemple 1 : débits d'un cours d'eau
df_scatter = pd.read_csv(os.path.join("data","scatter_plot.csv"), delimiter=",", index_col="t")
df_scatter.index = pd.to_datetime(df_scatter.index, format='%Y-%m-%d')

# Exemple 2 : tableau de corrélation des années/bassins
df_table = pd.read_csv(os.path.join("data","table.csv"), delimiter=",", index_col=0)

# Exemple 3 : localisation des différentes stations de débits
df_map = pd.read_csv(os.path.join("data","map.csv"), delimiter=";")

def getColorForValue(value, maxi):
    if value < maxi/3:
        factor = value / (maxi/3)
        r = 255
        g = 255 * factor
        b = 0
    elif value >= maxi/3 and value < 2*maxi/3:
        factor = (value - maxi/3) / (2*maxi/3)
        r = 255
        g = (1-factor) * 255
        b = 0
    else:
        factor = (value) / (maxi)
        r = (1-factor) * 255
        g = 255
        b = 0
    return 'rgb({0}, {1}, {2})'.format(r,g,b)

app = flask.Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


class Lien():
    def __init__(self) -> None:
        self.dico = None
        self.resultat = json.dumps([random.randint(1,10) for _ in range(10)])

    def set_lien(self, o):
        self.dico = xmltodict.parse(bytes(o).decode())
        self.save()

    def get_lien(self):
        return self.dico

    def save(self):
        with open('backEnd/donnee0.json', 'w') as fp:
            json.dump(self.dico, fp, indent=4)



    def chercherParametre(self, nom=''):
        #self.chercherEnProfondeur(self.dico['ParametersGroup'])
        if self.dico["ParametersGroup"]["ParametersGroup"][3]["Parameter"][1]["value"] != None:
            self.resultat = json.dumps([random.randint(1,int(self.dico["ParametersGroup"]["ParametersGroup"][3]["Parameter"][1]["value"])) for _ in range(int(self.dico["ParametersGroup"]["ParametersGroup"][3]["Parameter"][1]["value"]))])
        else:
            self.resultat = json.dumps([random.randint(1,int(self.dico["ParametersGroup"]["ParametersGroup"][3]["Parameter"][1]["default_value"])) for _ in range(int(self.dico["ParametersGroup"]["ParametersGroup"][3]["Parameter"][1]["default_value"]))])
        return self.resultat

    def chercherEnProfondeur(self, dictionnaire: dict):
        for k, v in dictionnaire.items():
            if k == 'ParametersGroup' or k == 'Parameter':
                for i in range(len(v)):
                    print(v[i]['@name'])
                    if v[i]['@name'] == "ndays_before_forecast":
                        if v[i]['value'] != None:
                            self.resultat = json.dumps([random.randint(1,int(v[i]['value'])) for _ in range(int(v[i]['value']))])
                        else:
                            self.resultat = json.dumps([random.randint(1,int(v[i]['default_value'])) for _ in range(int(v[i]['default_value']))])
                    else: self.chercherEnProfondeur(v[i])


            #     if v is list:
            #         Lien.chercherEnProfondeur(v[0])
            #     else:
            #         Lien.chercherEnProfondeur(v)
            # else:
            #     print(v)

lien = Lien()


@app.route('/osur/getxmlnames', methods=['GET'])
@cross_origin()
def xmlList():
    return flask.jsonify(["cydre_inputs", "PARADIS_inputs","run_global","perception_test"])

@app.route('/osur/getxml/PARADIS_inputs', methods=['GET'])
@cross_origin()
def paradis():
    xmlPath="PARADIS_A14_TRANSITION_MATRICE_1000.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/getxml/cydre_inputs', methods=['GET'])
@cross_origin()
def cydre():
    xmlPath="cydre_params.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/getxml/run_global', methods=['GET'])
@cross_origin()
def run_global():
    xmlPath="run_global.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/getxml/perception_test', methods=['GET'])
@cross_origin()
def perception_test():
    xmlPath="perception_test.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/get_rules/PARADIS_inputs', methods=['GET'])
@cross_origin()
def paradis_rules():
    xmlPath="PARADIS_A14_TRANSITION_MATRICE_1000_rules.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/get_rules/cydre_inputs', methods=['GET'])
@cross_origin()
def cydre_rules():
    xmlPath="cydre_params_rules.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/get_rules/run_global', methods=['GET'])
@cross_origin()
def run_global_rules():
    xmlPath="run_global_rules.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/get_rules/perception_test', methods=['GET'])
@cross_origin()
def perception_test_rules():
    xmlPath="perception_test_rules.xml"
    return flask.send_file(xmlPath, mimetype='application/xml')

@app.route('/osur/run_cydre', methods=['POST'])
@cross_origin()
def run_cydre():
    data = flask.request.get_data()

    # wait_times simule respectivement un temps d'attente random pour chaque étape.
    # Dans le cadre du projet ces temps d'attentes ne seront pas nécéssaire
    wait_times = [(0.01,0.5), (0.1,0.5), (0.5,0.7), (0.01,0.02),(0.7,1.3)]

    # success_strings simule les messages de succès pour chaque étape
    # Dans le cadre du projet ces messages seront générés par le code de cydre
    success_strings = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "Tableau de corrélation créé", "Carte générée", "Calculs finalisés", "Nettoyage terminé"]
    def generate():
        # Le premier yield défini le nombre d'étapes et le temps total estimé pour tout le processus
        yield "5|5:00\n"
        time.sleep(1)

        # On itére sur les étapes masi dans le vrai projet on aura plusieurs opérations séquentielles
        for step in range(5):
            # On sein d'une étape on va venir actualiser le temps restant et le pourcentage de complétion
            # yield "success_string|pourcentage|temps restant|moreDataBool|moreDataString\n"
            # On peut actualiser nos données de la manière suivante
            for i in range(20):
                # Dans le cas ou success_string est vide, on ne fait pas passer l'étape et on actualiser simplement le pourcentage et le temps restant
                # Ici on actualise le pourcentage de complétion
                yield "|"+str(20*step+i+1)+"||\n"
                time.sleep(random.uniform(wait_times[step][0], wait_times[step][1]))
                if (i==10):
                    # Ici on actualise le temps restant
                    yield "||"+str(4-step)+":30|\n"

            # Ici on fait passer l'étape et on actualise le pourcentage et le temps restant
            # Pour l'étape 2 on met moreDataBool à True et on ajoute un moreDataString qui va faire passer des données au boutton show
            yield success_strings[step]+"|"+str(20*(step+1))+"|"+str(4-step)+":00|"+str(step==1).lower()+"|"+("More DATA for step 2" if step == 1 else "") #first step have moredata (ex: plot,graph...)

    return flask.Response(flask.stream_with_context(generate()))

# @app.route('/osur/getJson', methods=['GET'])
# @cross_origin()
# def get_json():
#     json_list = []
#
#     with open("data/river_flow_data.csv", 'r') as csv_file:
#         csv_reader = csv.DictReader(csv_file, delimiter=',')
#
#         for row in csv_reader:
#             # Ajout de la valeur aléatoire entre -0.2 et +3 à Q
#             random_value = random.normalvariate(1, 0.2)
#             Q_with_random = float(row['Q']) + random_value
#
#             json_object = {
#                 't': row['t'],
#                 'Q': Q_with_random
#             }
#
#             json_list.append(json_object)
#
#     return json_list

@app.route('/osur/getJson', methods=['GET'])
@cross_origin()
def get_json():
    start_date = datetime(1969, 1, 1)
    end_date = datetime(2023, 9, 17)
    min_value = 0.8
    max_value = 8

    json_list = []
    river_flow_data = generate_river_flow_data(start_date, end_date, min_value, max_value)


    for row in river_flow_data:

        json_object = {
            't': row[0],
            'Q': row[1]
        }

        json_list.append(json_object)

    return json_list
def generate_river_flow_data(start_date, end_date, min_value, max_value):
    current_date = start_date
    data = [['Date', 'Debit']]
    previous_debit = random.uniform(min_value, max_value)
    while current_date <= end_date:
        formatted_date = current_date.strftime('%Y-%m-%d')

        # Ajouter une variation réaliste
        daily_variation = random.uniform(-0.3, 0.3)
        current_debit = max(min(previous_debit + daily_variation, max_value), min_value)

        data.append([formatted_date, current_debit])

        previous_debit = current_debit
        current_date += timedelta(days=1)

    return data

@app.route('/osur/getCoordinates', methods=['GET'])
@cross_origin()
def get_coordinates():
    # We don't use this one yet
    request_id = flask.request.args.get('ID')

    json_list = []

    with open("data/map.csv", 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=';')

        for row in csv_reader:
            json_object = {
                'name': row['name'],
                'ID': row['ID'],
                'x_outlet': row['x_outlet'],
                'y_outlet': row['y_outlet'],
                'Area': row['Area']
            }
            json_list.append(json_object)

    return json_list

@app.route('/osur/history', methods=['POST'])
@cross_origin()
def get_history():
    data = flask.request.get_json()
    filters = data['filters'] if 'filters' in data else []
    sort = data['sort'] if 'sort' in data else 'name'
    return flask.jsonify(
        sorted(sorted([Simulation(f.name, get_date(f), sort, find_value(next(f.glob('*.xml')), sort))
                for f in HISTORY_FOLDER.iterdir() if f.is_dir() and filter_xml(next(f.glob('*.xml')), filters)],
               key=lambda x: x[sort]),key=lambda x: x['favorite'], reverse=True))


@app.route('/osur/history/properties', methods=['GET'])
@cross_origin()
def get_properties():
    properties = set()
    for f in HISTORY_FOLDER.iterdir():
        if f.is_dir():
            xml_file = next(f.glob('*.xml'))
            properties.update(get_available_properties(xml_file))
    return flask.jsonify(['name', 'date'] + list(properties))


@app.route('/osur/history/<name>', methods=['GET'])
@cross_origin()
def get_simulation_xml(name):
    try:
        return flask.send_file(get_xml(name), mimetype='application/xml')
    except MissingSimulationError:
        return f'Missing simulation {name}', 400


@app.route('/osur/history/csv/<name>', methods=['GET'])
@cross_origin()
def get_simulation_csv(name):
    try:
        return flask.send_file(get_csv(name), mimetype='text/csv')
    except MissingSimulationError:
        return f'Missing simulation {name}', 400

@app.route('/osur/result', methods=['GET'])
@cross_origin()
def result():
    #return flask.jsonify()
    return flask.Response(lien.chercherParametre(), mimetype='text/plain')




@app.route('/osur/history/<name>', methods=['PUT'])
@cross_origin()
def rename_simulation_with_name(name):
    data = flask.request.get_json()
    if 'new_name' not in data:
        return 'Missing new name property', 400
    new_name = data['new_name']
    try:
        rename_simulation(name, new_name)
    except MissingSimulationError:
        return f'Missing simulation {name}', 400
    return '', 200


@app.route('/osur/history/<name>', methods=['DELETE'])
@cross_origin()
def delete_simulation_with_name(name):
    try:
        delete_simulation(name)
    except MissingSimulationError:
        return f'Missing simulation {name}', 400
    return '', 200


@app.route('/osur/history/<name>/<favorite>', methods=['PUT'])
@cross_origin()
def set_favorite_simulation_with_name(name, favorite: str):
    favorite = favorite.lower() == 'true'
    try:
        set_favorite(name) if favorite else unset_favorite(name)
    except MissingSimulationError:
        return f'Missing simulation {name}', 400
    return flask.jsonify(favorite), 200


# --- Génération des sorties python ---

# -- Exemple 1 : Courbe simple --

@app.route('/osur/display_scatter_plot', methods=['GET'])
@cross_origin()
def plot_scatter(df_scatter=df_scatter):
    """
    # fig = px.line(
    #     x=df_scatter.index,
    #     y=df_scatter["Q"],
    #     template="simple_white",
    #     labels={"x": "Date", "y": "Débit (m3/s)"},
    # )

    # fig.update_traces(line=dict(width=1, color='#5593c8'))

    # fig.update_layout(
    #   title="Test de l'affichage d'un graphique simple",
    #   title_x=0.5,
    #   plot_bgcolor="rgba(0,0,0,0)",
    #   paper_bgcolor="rgba(0,0,0,0)")

    # response = flask.jsonify(render_template(template_name_or_list="map.html",plot=fig.to_html()))
    # response.headers.add('Content-Type', 'text/html')
    # response = flask.jsonify(fig.to_dict())
    """
    return flask.jsonify({'x': [str(dt) for dt in df_scatter.index], 'y': [float(dt) for dt in df_scatter["Q"]]})




# -- Exemple 2 : Tableau au format json --

@app.route('/osur/display_table', methods=['GET'])
@cross_origin()
def table(df_table=df_table):

    # # response = df_table.to_json(orient='split')
    values = {}
    for k,v in dict(df_table).items():  values.update({k : [float(i) for i in v.values]})
    return flask.jsonify({'x': [str(i) for i in df_table.index], 'y': values})





# -- Exemple 3 : Carte de la localisation des stations de mesure des débits --

@app.route('/osur/display_map', methods=['GET'])
@cross_origin()
def plot_map(df_map=df_map):
    # print(list(df_map['ID']))
    """
    # fig = px.scatter_mapbox(df_map, lon=df_map['x_outlet'], lat=df_map['y_outlet'],
    #                         hover_name = df_map["name"])

    # # Update the layout
    # fig.update_layout(mapbox_style="open-street-map",
    #                   mapbox_center={"lat": 48.2141667, "lon": -2.9424167},
    #                   mapbox_zoom=6.8,
    #                   paper_bgcolor="rgba(0,0,0,0)",
    #                   margin={"l": 0, "r": 0, "t": 0, "b": 0})

    # response = flask.jsonify(render_template(template_name_or_list="map.html",plot=fig.to_html()))
    # response.headers.add('Content-Type', 'text/html')
    # response = flask.jsonify(fig.to_dict())

    # response = pio.write_html(fig, file=os.path.join("backEnd","ma_figure_mapbox.html"))
    # return flask.send_file("ma_figure_mapbox.html", mimetype='text/html')
    """
    return flask.jsonify({'name': list(df_map["name"]), 'lo': list(df_map['x_outlet']), 'la': list(df_map['y_outlet']), 'id':list(df_map['ID']), 'area': [max(x/20,7) for x in df_map['Area']], 'color': [getColorForValue(x, max(df_map['Area'])) for x in df_map['Area']]})





@app.route('/osur/display_map2', methods=['GET'])
@cross_origin()
def plot_map2(df_map=df_map):
    figure = go.Figure(data=[go.Scatter(x=df_map['x'], y=df_map['y'], mode='lines+markers')])
    return flask.jsonify(figure)



if __name__ == '__main__':
    app.run(host='localhost')

