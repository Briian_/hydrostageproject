import json
import dicttoxml

# Charger le fichier JSON
with open('bass.json') as json_file:
    data = json.load(json_file)

# Convertir le JSON en XML
xml_data = dicttoxml.dicttoxml(data)

# Enregistrer le résultat dans un fichier XML
with open('bass.xml', 'wb') as xml_file:
    xml_file.write(xml_data)
